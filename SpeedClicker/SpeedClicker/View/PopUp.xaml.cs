﻿using SpeedClicker.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SpeedClicker
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUp 
    {
        

        public PopUp(int Combustible)
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel(1,Combustible);
            
        }
               
    }
}