﻿using SpeedClicker.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SpeedClicker.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tutorial : ContentPage
    {
        public Tutorial()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel();
        }
        public Tutorial(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage());
        }

    }
}