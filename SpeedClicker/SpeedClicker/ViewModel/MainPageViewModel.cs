﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SpeedClicker.ViewModel 
{
    class MainPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        //Aqui estan los comandos
        public ICommand combustibleInc { get; set; }
        public ICommand comandoCronometro { get; set; }
        //Variables int
        int combustible = 0;
        int cronometro = 15;
        int ayudaDis = 1;
        int boolPop=0;
        int movimientoNaveY = 100;
        int distancia = 1;
        int movimientoFuegoY = 100;      
        int movimientoNaveX = 0;
        int movFondoX = 0;
        int movimientoFuegoX = 0;
        
        //Variables dounle
        double progress2 = 0.0;
        double progress = 0.0;

        //Variables Bool
        bool fuego = false;
        bool enabled = true;
        bool progVis = false;

        //Variables String
        String fondo = "estacion.png";
        String colorBar = "Green";
        String colorBar2 = "Green";

        //Variables Boolean
        Boolean habilitat = true ;

        //CONSTRUCTOR NUMERO 1 MAINPAGE
        public MainPageViewModel()
        {
        
            combustibleInc = new Command(combustibleIncremento);
            comandoCronometro = new Command(Cronometro1);
            MovimientoFuegoY = MovimientoNaveY - 60;
            comprobacion1();
            Cronometro1();
            progresBar();
            PopUp();
            Combustible = 0;
        }
        //CONSTRUCTOR NUMERO 2 MAINPAGE CON PARAMETROS PARA RECIBIR
        public MainPageViewModel(int ji,int Combustible)
        {
            boolPop = ji;
            this.Combustible = Combustible;
            combustibleInc = new Command(combustibleIncremento);
            comandoCronometro = new Command(Cronometro1);
            MovimientoFuegoY = MovimientoNaveY - 60;
            comprobacion1();
            Cronometro1();
            progresBar();
            PopUp();
            Combustible = 0;
        }
        
        
        //GETTERS Y SETTERS DE LAS VARIABLES A BINDEAR
        public String Fondo
        {
            set { if (fondo != value) { fondo = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Fondo")); } } }
            get
            {

                return fondo;
            }
        }
        public String ColorBar
        {
            set { if (colorBar != value) { colorBar = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("ColorBar")); } } }
            get
            {

                return colorBar;
            }
        }
        public String ColorBar2
        {
            set { if (colorBar2 != value) { colorBar2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("ColorBar2")); } } }
            get
            {

                return colorBar2;
            }
        }
        public double Progress
        {

            set { if (progress != value) { progress = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Progress")); } } }
            get
            {

                return progress;
            }
        }
        public double Progress2
        {

            set { if (progress2 != value) { progress2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Progress2")); } } }
            get
            {

                return progress2;
            }
        }
        public int Combustible
        {

            set { if (combustible != value) { combustible = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Combustible")); } } }
            get
            {

                return combustible;
            }
        }
  
        public int Cronometro
        {

            set { if (cronometro != value) { cronometro = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Cronometro")); } } }
            get
            {
                 
                return cronometro;
            }
        }
        public int MovimientoNaveY
        {

            set { if (movimientoNaveY != value) { movimientoNaveY = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("MovimientoNaveY")); } } }
            get
            {

                return movimientoNaveY;
            }
        }
        public int MovimientoFuegoX
        {

            set { if (movimientoFuegoX != value) { movimientoFuegoX = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("MovimientoFuegoX")); } } }
            get
            {

                return movimientoFuegoX;
            }
        }
        public int MovFondoX
        {

            set { if (movFondoX != value) { movFondoX = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("MovFondoX")); } } }
            get
            {

                return movFondoX;
            }
        }
        public int MovimientoFuegoY
        {

            set { if (movimientoFuegoY != value) { movimientoFuegoY = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("MovimientoFuegoY")); } } }
            get
            {

                return movimientoFuegoY;
            }
        }
        public int MovimientoNaveX
        {

            set { if (movimientoNaveX != value) { movimientoNaveX = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("MovimientoNaveX")); } } }
            get
            {

                return movimientoNaveX;
            }
        }
        public bool Fuego
        {

            set { if (fuego != value) { fuego = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Fuego")); } } }
            get
            {

                return fuego;
            }
        }
        public bool ProgVis
        {

            set { if (progVis != value) { progVis = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("ProgVis")); } } }
            get
            {

                return progVis;
            }
        }
        public bool Enabled
        {

            set { if (enabled != value) { enabled = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Enabled")); } } }
            get
            {

                return enabled;
            }
        }

        //A PARTIR DE AQUI COMPROBAMOS QUE EL CRONOMETRO ESTA A 0, Y SI ESTA A 0 HACEMOS QUE LA NAVE VAYA AUMENTANDO HACIA ARRIBA, CUANDO LA NAVE LLEGUE A X POSICION DE Y, --
        // -- HACEMOS QUE LA NAVE VUELVA ABAJO DE LA PANTALLA PARA QUE VUELVA A SUBIR, LA NAVE HACEMOS QUE RECORRA X DISTANCIA DEPENDIENDO DE LA VARIABLE COMBUSTIBLE.
        public async Task comprobacion1()
        {
            int contador = 0;
            while (distancia > 0)
            {
                if (Cronometro==0)
                {
                    
                    despegue();
                    MovimientoFuegoY = MovimientoFuegoY-25;
                    distancia = Combustible * 3;               
                    await Task.Delay(700);
                
                    while (distancia > 0) {
                     
                     
                        for (int i = 0; i < 5; i++)
                        {
                            MovimientoNaveY = MovimientoNaveY - 7;
                            MovimientoFuegoY = MovimientoNaveY - 35;
                            ayudaDis = distancia;

                            if (MovimientoNaveY < -650)
                            {
                                if (contador==0)
                                {
                                    Fondo = "sky.png";
                                    contador++;
                                    ProgVis = false;
                                }
                                else if (contador==1)
                                {
                                    Fondo = "espacio2.png";
                                    contador++;
                                }
                                else if (contador == 2)
                                {
                                    Fondo = "espacio.jpg";
                                    contador++;
                                }
                                else if (contador == 3)
                                {
                                    Fondo = "espacio3.png";
                                    contador++;
                                }
                                else if (contador == 4)
                                {
                                    Fondo = "espacio4.png";
                                    contador++;
                                }
                                else if (contador == 5)
                                {
                                    Fondo = "espacio5.png";
                                   
                                }



                                //MovimientoFuegoY = 650;
                                MovimientoNaveY = 685;
                            }
                            await Task.Delay(1);
                        }
                        distancia = distancia - 1;
                        ayudaDis = ayudaDis - 1;

                        Enabled = false;
                        ProgVis = false;

                        await Task.Delay(5);
                        
                    }

                }
                await Task.Delay(100);
            }
            
                
            
        }
        //FUNCION QUE HACE QUE EL JUEGO VIBRE CUANDO LE DES
        async void animacionFuego()
        {
            for(int i = 0; i < 5; i++)
            {
                MovimientoFuegoX = -15;
                await Task.Delay(10);
                MovimientoFuegoX = 15;
                await Task.Delay(10);
                MovimientoFuegoX = -10;
                await Task.Delay(10);
                MovimientoFuegoX = 10;
                await Task.Delay(10);
                MovimientoFuegoX = -5;
                await Task.Delay(10);
                MovimientoFuegoX = 5;
            }         
        }
        //ANIMACION QUE HACE QUE LA NAVE VIBRE CUANDO SEA CLICKADA, TAMBIEN QUE LA PROGRESS BAR VAYA SUBIENDO DEPENDIENDO DE LOS CLICKS QUE LE DES, Y SI LLEGA A X NUMERO, CAMBIA DE COLOR Y
        // SE AÑADE OTRA PROGRESS BAR
         async void combustibleIncremento()
        {
            Fuego = true;
            if (habilitat == true) { 
            animacionFuego();
            for (int i = 0; i < 5; i++)
            {
                MovimientoNaveX = -15;
                await Task.Delay(10);
                MovimientoNaveX = 15;
                await Task.Delay(10);
                MovimientoNaveX = -10;
                await Task.Delay(10);
                MovimientoNaveX = 10;
                await Task.Delay(10);
                MovimientoNaveX = -5;
                await Task.Delay(10);
                MovimientoNaveX = 5;
            }
            }
            if (Cronometro<=0)
            {
               
                Combustible = Combustible;
                
            }
            else if (ProgVis==false)
            {
                Combustible = Combustible + 1;
               
                Progress = Progress + 0.05;
                if (Progress > 0.3)
                {
                    ColorBar = "Yellow";
                }
                if (Progress > 0.6)
                {
                    ColorBar = "Orange";
                }
                if (Progress > 0.9)
                {
                    ColorBar = "red"; 
                }
            }
            else
            {
                Combustible = Combustible + 1;
           
                Progress2 = Progress2 + 0.05;
                
                if (Progress2 > 0.3)
                {
                    ColorBar2 = "Yellow";
                }
                if (Progress2 > 0.6)
                {
                    ColorBar2 = "Orange";
                }
                if (Progress2 > 0.9)
                {
                    ColorBar2 = "red";
                }
            }  
        }
        //FUNCION QUE HACE QUE LA PROGRESS BAR VAYA BAJANDO, PARA AGREGAR DIFICULTAD AL JUEGO
        public async Task progresBar()
        {
            while (Progress<1)
            {
                Progress = Progress - 0.01;
                if (Progress<0)
                {
                    Progress = 0;
                }
                await Task.Delay(50);
            }
            progresBar2();
        }
        //FUNCION QUE HACE QUE EL FONDO HAGA ANIMACION DE VIBRAR
        async void despegue()
        {
            Vibracio();

            for (int i = 0; i < 5; i++)
            {
                MovFondoX = -15;
                await Task.Delay(5);
                MovFondoX = 15;
                await Task.Delay(5);
                MovFondoX = -10;
                await Task.Delay(5);
                MovFondoX = 10;
                await Task.Delay(5);
                MovFondoX = -5;
                await Task.Delay(5);
                MovFondoX = -4;                
            }
            MovFondoX = 1;
            habilitat = false;


        }
        //FUNCION QUE HACE QUE LA PROGRESS BAR 2 VAYA BAJANDO PARA AGREGAR DIFICULTAD
       public async Task progresBar2()
        {
            ProgVis = true;
            MovimientoFuegoY = MovimientoFuegoY - 25;
            while (Progress2 < 1)
            {
                Progress2 = Progress2 - 0.01;
                if (Progress2 < 0)
                {
                    Progress2 = 0;
                }
                await Task.Delay(50);
            }
        }
        //LLAMAMOS A CRONOMETRO2
        public void Cronometro1()
        {
            cronometro2();
        }
        //FUNCION QUE HACE UN CRONOMETRO, RESTA 1 CADA SEGUNDO
        public async Task cronometro2()
        {

            while (Cronometro>0)
            {
                Cronometro = Cronometro - 1;
               
                await Task.Delay(1000);

            }
            MovimientoFuegoY = MovimientoFuegoY + 25;


        }
   
        //FUNCOIN DE POPUP, CUANDO ACABE LA PARTIDA LLAMAREMOS A ESTE POPUP   
        private async void PopUp()
        {


            if (boolPop!=1) {
                while (ayudaDis > 0)
                {
                    await Task.Delay(100);

                }

                var pr = new PopUp(Combustible);
                var scaleAnimation = new ScaleAnimation
                {
                    PositionIn = MoveAnimationOptions.Right,
                    PositionOut = MoveAnimationOptions.Left
                };
                pr.Animation = scaleAnimation;
                await PopupNavigation.PushAsync(pr);
                
            }
        }
  
       public void Vibracio() //Aquesta funcio el que fa es fer vibrar el mobil.
        {
            try
            {
                // Use default vibration length
                Vibration.Vibrate();

                // Or use specified time
                var duration = TimeSpan.FromSeconds(1.5); //Duracio de la vibracio
                Vibration.Vibrate(duration);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Feature not supported on device
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }

    }
}
